package com.conflowence.in;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class FolderIDFormatter extends AbstractMediator{

	@Override
	public boolean mediate(MessageContext ctx) {
		
		String folderId = ctx.getProperty("gDriveFolderId").toString();
		System.out.println("Folder ID : "+folderId);
		
		folderId = "\"[" + folderId + "]\"";
		System.out.println("Formatted Folder ID : "+folderId);
		
		ctx.setProperty("gDriveFolderId", folderId);
		return true;
	}
	
	

}

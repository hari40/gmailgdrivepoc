package com.conflowence.in;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class EmailValidation extends AbstractMediator {
	
	@Override
	public boolean mediate(MessageContext context) {
		if(checkEmailExistance(context)) {
			context.setProperty("EmailFoundFlag", "PASS");
		}else {
			context.setProperty("EmailFoundFlag", "FAIL");
		}
		return true;
	}
	
	public boolean checkEmailExistance(MessageContext context) {
		System.out.println("Inside Email Validation Flow");
		String inputEmail = context.getProperty("inputRequest").toString();
		String msgEmail = context.getProperty("fromEmail").toString();
		System.out.println("Input Email for verification : "+inputEmail);
		System.out.println("Message Email for verification : "+msgEmail);
		String[] inputEmails = inputEmail.split("\\^");
		for(int i=0; i<inputEmails.length; i++) {
			if(msgEmail.replaceAll(" ", "").toUpperCase().contains(inputEmails[i].replaceAll(" ", "").toUpperCase())) {
				//System.out.println("A match found");
				return true;
			}
		}
		
		return false;
	}

}
package com.conflowence.in;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;

public class DriveFolderFormatter extends AbstractMediator{

	@Override
	public boolean mediate(MessageContext ctx) {
		String inputFileName = ctx.getProperty("filename").toString();
		String folderName = inputFileName.split("-")[0];
		System.out.println("Folder Name : "+folderName);
		ctx.setProperty("folderName", folderName);
		return true;
	}

}


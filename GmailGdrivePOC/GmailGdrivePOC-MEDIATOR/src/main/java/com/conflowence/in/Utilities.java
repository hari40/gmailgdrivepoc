package com.conflowence.in;

import org.apache.synapse.MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.json.JSONArray;

public class Utilities extends AbstractMediator { 

	public boolean mediate(MessageContext context) { 
		parseInputJson(context);
		return true;
	}
	
	/* Method to Parse the input json*/
	public void parseInputJson(MessageContext context) {
		System.out.println("Inside Input Parser");
		System.out.println("Input JSON : " + context.getProperty("inputRequest").toString());
		
		String[] inputEmails = context.getProperty("inputRequest").toString().split("\\^");
		System.out.println("Input Emails : "+inputEmails.length);
		
		JSONArray inputEmailArray = new JSONArray();
		for (int i=0; i<inputEmails.length; i++) {
			inputEmailArray.put(inputEmails[i]);
		}
		
		System.out.println("Parsed JSON Array : "+inputEmailArray.toString(2));
		
		context.setProperty("ParsedInputEmails", inputEmailArray.toString());
		
	}
}
